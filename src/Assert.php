<?php
namespace Bx\Assert;
use Webmozart\Assert\Assert as BaseAssert;

/**
 * Содержит методы для проверки значений на допустимость. Является расширением библиотеки
 * webmozart/assert.
 * @link https://github.com/webmozart/assert
 */
class Assert extends BaseAssert
{
    
}
